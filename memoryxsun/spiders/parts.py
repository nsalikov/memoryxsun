# -*- coding: utf-8 -*-
import re
import scrapy
from scrapy.spiders import SitemapSpider


class PartsSpider(SitemapSpider):
    name = 'parts'
    allowed_domains = ['memoryxsun.com', 'mxsun.stores.yahoo.net']
    sitemap_urls = ['https://www.memoryxsun.com/sitemap.xml']


    def sitemap_filter(self, entries):
        for entry in entries:
            entry['loc'] = entry['loc'].replace('mxsun.stores.yahoo.net', 'www.memoryxsun.com').replace('http:', 'https:')
            yield entry


    def parse(self, response):
        d = {}

        d['url'] = response.request.url
        d['name'] = response.css('.productH1 ::text').extract_first()

        if d['name']:
            d['name'] = d['name'].strip()

        d['breadcrumbs'] = list(filter(None, [t.strip() for t in response.css('td[width="650"] table font a ::text').extract()]))
        d['breadcrumbs_urls'] = list(filter(None, [t.strip() for t in response.css('td[width="650"] table font a ::attr(href)').extract()]))

        if len(d['breadcrumbs_urls']) >= 2 and d['breadcrumbs_urls'][0] == 'index.html' and d['breadcrumbs_urls'][1] == 'index.html':
            d['breadcrumbs_urls'].pop(0)

        d['raw'] = list(self.get_raw_specs(response))
        d['specs'] = {}
        d['comments'] = []

        if not d['raw']:
            self.logger.warning('Unable to find/parse specs: {}'.format(response.request.url))
            yield d
        else:
            d['specs'], d['comments'] = parse_raw_specs(d['raw'])
            yield d


    def get_raw_specs(self, response):
        trs_xpath = '//td[contains(@bgcolor, "#ebebeb") or contains(@bgcolor, "#fce2e2")]/ancestor::tr[1]'
        for tr in response.xpath(trs_xpath):
            if tr.css('tr td ul li'):
                for li in tr.css('tr td ul li'):
                    if li.css('li br'):
                        yield from handle_li_br(li.css('li'))
                    else:
                        text = li.css('li ::text').extract()
                        text = '\n'.join(filter(None, [re.sub('[\n\r\xa0 ]+', ' ', t, flags=re.DOTALL).strip('- ') for t in text]))
                        yield text
            if tr.css('tr td br'):
                yield from handle_td_br(tr.css('tr td'))
            elif tr.css('tr td'):
                tds = list(clean_tds(tr.css('tr td')))
                if len(tds) == 1:
                    yield tds[0]
                elif len(tds) >= 2:
                    yield tds[0].strip(': ') + ':::' + ' '.join(tds[1:])

        bbrown_css = 'center+font>table>tr>td>p.arial-12BBrown'
        if response.css(bbrown_css):
            yield from parse_bbrown(response)


def clean_tds(tds):
    for td in tds:
        text = td.css('td ::text').extract()
        text = ' '.join(filter(None, [re.sub('[\n\r\xa0 ]+', ' ', t, flags=re.DOTALL).strip('- ') for t in text]))
        yield text


def handle_li_br(elements):
    for element in elements:
        text_lst = element.xpath('./*/text()[preceding-sibling::br or following-sibling::br]').extract()
        text_lst = list(filter(None, [re.sub('[\n\r\xa0 ]+', ' ', t, flags=re.DOTALL).strip('- ') for t in text_lst]))

        if len(text_lst) == 1:
            yield text_lst[0]
        elif len(text_lst) >= 2:
            yield text_lst[0].strip(': ') + ':::' + '\n'.join(text_lst[1:])


def handle_td_br(elements):
    for element in elements:
        text_lst = element.xpath('./*/text()[preceding-sibling::br or following-sibling::br]').extract()
        text_lst = list(filter(None, [re.sub('[\n\r\xa0 ]+', ' ', t, flags=re.DOTALL).strip('- ') for t in text_lst]))

        for t in text_lst:
            if t.count(':') == 1:
                yield re.sub('\s*:\s*', ':::', t)
            else:
                yield t


def parse_bbrown(response):
    nodes_text = 'center+font>table>tr>td:only-child ::text'
    text_lst = list(filter(None, [re.sub('[\n\r\t\xa0 ]+', ' ', t, flags=re.DOTALL).strip('- ') for t in response.css(nodes_text).extract()]))
    text_lst = [t for t in text_lst if t != '*Click on picture for a better view']

    headers = [
        'Description',
        'Features',
        'Dimensions',
        'Product Specs',
        'Weight',
        'Includes',
        'Notes',
        'Compatibility',
        'Warranty',
    ]

    current_header = ''
    current_texts = []

    for text in text_lst:
        if text in headers:
            if current_header == '':
                current_header = text
            else:
                yield current_header + ':::' + '\n'.join(current_texts)
                current_header = text
                current_texts = []
        else:
            current_texts.append(text)


def parse_raw_specs(data):
    specs = {}
    comments = []

    for line in data:
        if ':::' in line:
            key, value = line.split(':::')
            specs[key] = list(filter(None, value.split('\n')))
        else:
            comments.append(line)

    return specs, comments
